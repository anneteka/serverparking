using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServerParking.ParkingObjects;
using ServerParking.Services;

namespace ServerParking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        // GET api/cars
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            List<Car> cars = ParkingLot.GetInstance().Cars;
            string[] ret = new string[cars.Count];
            for (int i = 0; i < ret.Length; i++)
            {
                ret[i] = cars[i].ToString();
            }

            return ret;
        }

        // GET api/cars/id
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return ParkingLot.GetInstance().Cars[id].ToString();
        }

        // POST api/cars
        [HttpPost]
        public Car CreateCar([FromBody] string car)
        {
            return CarService.GetInstance().AddCar(car);
        }

        //PUT api/cars/id
        [HttpPut("{id}/{amount}")]
        public double PutMoney(int id, int amount)
        {
           return CarService.GetInstance().IncreaseMoney(id, amount);
        }

        // DELTE api/car/id
        [HttpDelete("{id}")]
        public bool DeleteCar(string id)
        {
            return CarService.GetInstance().DeleteCar(id);
        }
    }
}