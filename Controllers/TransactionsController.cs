using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ServerParking.ParkingObjects;
using ServerParking.Services;

namespace ServerParking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        // GET api/transactions
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            List<Transaction> transactions = ParkingLot.GetInstance().Transactions;
            string[] ret = new string[transactions.Count];
            for (int i = 0; i < ret.Length; i++)
            {
                ret[i] = transactions[i].ToString();
            }

            return ret;
        }

        // GET api/transactions/id
        [HttpGet("{id}")]
        public ActionResult<Transaction> Get(int id)
        {
            return ParkingLot.GetInstance().Transactions[id];
        }

        // POST api/transactions
        [HttpPost]
        public Transaction CreateTransaction([FromBody]string transaction)
        {
            return TransactionService.GetInstance().AddTransaction(transaction);
        }
        
        // DELTE api/car/id
        [HttpDelete("{id}")]
        public bool DeleteCar(string id)
        {
            return TransactionService.GetInstance().DeleteTransaction(id);
        }
    }
}