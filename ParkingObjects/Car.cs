using System;
using System.CodeDom.Compiler;


namespace ServerParking.ParkingObjects
{
    public class Car
    {
        private static int carAmount = 0;
        private int type;
        private int id;
        private double moneyAmount;

        public Car(int type, double money)
        {
            id = carAmount++;
            moneyAmount = money;
            this.type = type;
        }
        
        public int Type
        {
            get => type;
            set => type = value;
        }

        public int Id
        {
            get => id;
            set => id = value;
        }

        public double MoneyAmount
        {
            get => moneyAmount;
            set => moneyAmount = value;
        }

        public Transaction pay()
        {
            double withdraw = 0;
            switch (type)
            {
                case Settings.LightCar:
                    withdraw = Settings.LightCarPrice;
                    break;
                case Settings.Bus:
                    withdraw = Settings.BusPrice;
                    break;
                case Settings.Motorcycle:
                    withdraw = Settings.MotorcyclePrice;
                    break;
                case Settings.Truck:
                    withdraw = Settings.TruckPrice;
                    break;
            }

            double ret = moneyAmount < withdraw ? withdraw * 2.5 : withdraw;
            moneyAmount -= ret;
            return new Transaction(id, ret, DateTime.Now);
        }

        public override bool Equals(object obj)
        {
            if (id == ((Car) obj).id)
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

     

        public override string ToString()
        {
            return "ID: " + id + ", type: " + type.ToString() + ", money: " + moneyAmount;
        }
    }
}