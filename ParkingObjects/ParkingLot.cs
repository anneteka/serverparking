using System;
using System.Collections.Generic;

namespace ServerParking.ParkingObjects
{
    public class ParkingLot
    {
        private static ParkingLot instance;

        private List<Transaction> transactions;
        private List<Car> cars;
        private int balance;

        private ParkingLot()
        {
            transactions = new List<Transaction>();
            cars = new List<Car>();
            balance = 0;
        }

        public static ParkingLot GetInstance()
        {
            if (instance == null)
                instance = new ParkingLot();
            return instance;
        }

        public List<Transaction> Transactions
        {
            get => transactions;
            set => transactions = value;
        }

        public List<Car> Cars
        {
            get => cars;
            set => cars = value;
        }

        public int Balance
        {
            get => balance;
            set => balance = value;
        }
    }
}