using System;

namespace ServerParking.ParkingObjects
{
    public class Transaction
    {
        private DateTime time;
        private int carID;
        private double moneyWithdrawn;
        private int id;
        private static int transactionsAmount = 0;
        
        public Transaction(int carID, double money, DateTime time)
        {
            this.time = time;
            this.carID = carID;

            moneyWithdrawn = money;
            id = transactionsAmount++;
        }

        public override string ToString()
        {
            return "ID: "+id+", car ID: "+carID+", time: "+time.Hour+":"+time.Minute+":"+time.Second+", money: "+moneyWithdrawn;
        }

        public double MoneyWithdrawn
        {
            get => moneyWithdrawn;
            private set => moneyWithdrawn = value;
        }

        public int Id
        {
            get => id;
            set => id = value;
        }
    }
}