namespace ServerParking.ParkingObjects
{
    public static class Settings
    {
        public static int ParkingBalanceDefault = 0;
        public static int MaxCarAmount = 10;
        public static int WithdrawalTime = 5;
        public static double LightCarPrice = 2;
        public static double TruckPrice = 5;
        public static double BusPrice = 3.5;
        public static double MotorcyclePrice = 1;
        public static double Fee = 2.5;

        public const int LightCar = 0;
        public const int Truck = 1;
        public const int Bus = 2;
        public const int Motorcycle = 3;
    }
}