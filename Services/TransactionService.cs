using System;
using ServerParking.ParkingObjects;

namespace ServerParking.Services
{
    public class TransactionService
    {
        private static TransactionService instance;

        private TransactionService()
        {
        }

        public void AddTransaction(Transaction Transaction)
        {
            ParkingLot.GetInstance().Transactions.Add(Transaction);
        }

        public Transaction AddTransaction(string Transaction)
        {
            string[] param = Transaction.Split(",");
            Transaction ret = new Transaction(Int32.Parse(param[0]), Double.Parse(param[1]),DateTime.Parse(param[2]));
            ParkingLot.GetInstance().Transactions.Add(ret);
            return ret;
        }

        public bool DeleteTransaction(string sid)
        {
            int id = Int32.Parse(sid);
            return ParkingLot.GetInstance().Transactions.Remove(ParkingLot.GetInstance().Transactions.Find(x => x.Id == id));
        }

        public static TransactionService GetInstance()
        {
            if (instance == null)
                instance = new TransactionService();
            return instance;
        }

    }
}