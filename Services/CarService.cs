using System;
using System.Collections.Generic;
using ServerParking.ParkingObjects;

namespace ServerParking.Services
{
    public class CarService
    {
        private static CarService instance;

        private CarService()
        {
        }

        public void AddCar(Car car)
        {
            ParkingLot.GetInstance().Cars.Add(car);
        }

        public Car AddCar(string car)
        {
            string[] param = car.Split(",");
            Car ret = new Car(Int32.Parse(param[0]), Double.Parse(param[1]));
            ParkingLot.GetInstance().Cars.Add(ret);
            return ret;
        }

        public bool DeleteCar(string sid)
        {
            int id = Int32.Parse(sid);
           return ParkingLot.GetInstance().Cars.Remove(ParkingLot.GetInstance().Cars.Find(x => x.Id == id));
        }

        public double IncreaseMoney(string sid, string money)
        {
            double pay = Double.Parse(money);
            int id = Int32.Parse(sid);
            Car car = ParkingLot.GetInstance().Cars.Find(x => x.Id == id);
            car.MoneyAmount += pay;
            return car.MoneyAmount;
        }
        public double IncreaseMoney(int id, int money)
        {
         
            Car car = ParkingLot.GetInstance().Cars.Find(x => x.Id == id);
            car.MoneyAmount += money;
            return car.MoneyAmount;
        }

      

        public static CarService GetInstance()
        {
            if (instance == null)
                instance = new CarService();
            return instance;
        }
        
    }
}